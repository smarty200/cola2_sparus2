# Changelog

## [20.10.0] - 26-10-2020

* Adapted configuration for new release

## [3.2.0] - 06-11-2019

* Add independent imu launchfile
* Delete node names from configurations
* Rename launchfiles to have common names between vehicles
* Applied cola2 lib refactor changes

## [3.1.0] - 25-02-2019

* First release
